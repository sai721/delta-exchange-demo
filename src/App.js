import { useEffect, useState } from "react";
import "./App.css";
import productSerice from "./service/product.service";
import RowDetails from "./components/row-details/RowDetails";
import Header from "./components/header/header";

function App() {
  const [data, setData] = useState(null);
  useEffect(() => {
    productSerice
      .findAll("page_size=" + 20)
      .then((res) => {
        console.log("recived ", res);
        setData(res.data.result);
      })
      .catch((err) => console.log(err));
    return () => {};
  }, []);

  return (
    <div className="App">
      <Header />
      <div className="body_container">
        {data &&
          data.map((row) => (
            <RowDetails row={row} />
          ))}
      </div>

      {/* <SinglePrice symbol="SHIBUSDT" /> */}
    </div>
  );
}

export default App;
