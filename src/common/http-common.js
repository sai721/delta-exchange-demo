import axios from "axios";

export default axios.create({
  baseURL: "https://api.delta.exchange/v2",
  headers: {
    "Content-type": "application/json",
  },
});
