import "./header.css";
const Header = () => {
  return (
    <div className="row_header">
      <div className="header_tab">Symbol</div>
      <div className="header_tab">Description</div>
      <div className="header_tab">Underlying Asset</div>
      <div className="header_tab">Market Price</div>
    </div>
  );
};

export default Header;
