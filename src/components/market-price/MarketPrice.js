import { useEffect, useState } from "react";
import "./market-price.css";
const priceFormatter = (price) => {
  return `$${price.toFixed(5)}`;
};
const emojis = {
  "": "",
  up: String.fromCodePoint(),
};
const SinglePrice = ({ symbol }) => {
  const [direction, setDirection] = useState("");
  const [previousPrice, setPreviousPrice] = useState(null);
  const [currentPrice, setCurrentPrice] = useState(null);
  useEffect(() => {
    const ws = new WebSocket("wss://production-esocket.delta.exchange");

    ws.onopen = function open() {
      console.log("connected");
      ws.send(
        JSON.stringify({
          payload: {
            channels: [
              {
                name: "mark_price",
                symbols: ["MARK:" + symbol],
              },
            ],
          },
          type: "subscribe",
        })
      );
    };

    ws.onclose = function close() {
      console.log("disconnected");
    };

    ws.onmessage = function incoming(message) {
      // console.log("comming message", message.data, JSON.parse(message.data));
      // setPrice(JSON.parse(message.data).price);

      const next = JSON.parse(message.data).price;
      // setPreviousPrice(currentPrice);
      setCurrentPrice((current) => {
        setPreviousPrice(current);
        return next;
      });
    };

    return () => {};
  }, []);

  useEffect(() => {
    // console.log(previousPrice, currentPrice);

    if (previousPrice && currentPrice) {
      const nextDirection =
        currentPrice > previousPrice
          ? "up"
          : previousPrice > currentPrice
          ? "down"
          : "";
      // console.log(nextDirection);
      if (nextDirection) {
        setDirection(nextDirection);
      }
    }
    return () => {};
  }, [previousPrice, currentPrice]);

  return (
    <>
      {currentPrice ? (
        <span
          className={`${
            direction == "up" ? "up" : direction == "down" ? "down" : ""
          }`}
        >
          {priceFormatter(parseFloat(currentPrice))}
        </span>
      ) : (
        "-"
      )}
    </>
  );
};

export default SinglePrice;
