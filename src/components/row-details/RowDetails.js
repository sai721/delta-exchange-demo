import MarketPrice from "../market-price/MarketPrice";
import "./row-styles.css";
const RowDetails = ({ row }) => {
  return (
    <>
      <div className="row_container">
        <div className="row_col">{row.symbol}</div>
        <div className="row_col">{row.description}</div>
        <div className="row_col">{row.underlying_asset.symbol}</div>
        <div className="row_col">
          <MarketPrice symbol={row.symbol} />
        </div>
      </div>
    </>
  );
};

export default RowDetails;
