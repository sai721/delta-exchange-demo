import http from "../common/http-common";

export class ProductService {
  findAll(query) {
    return http.get("/products?" + (query ? query : ""));
  }
}

export default new ProductService();
